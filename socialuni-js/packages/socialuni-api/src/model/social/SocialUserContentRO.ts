import SocialuniUserRO from "../user/SocialuniUserRO";

export default class SocialUserContentRO extends SocialuniUserRO {
    hasFollowed: boolean = null
    identityAuth: boolean = null
    hasBeFollowed: boolean = null
}
