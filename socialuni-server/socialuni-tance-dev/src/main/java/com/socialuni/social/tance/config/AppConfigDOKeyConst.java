package com.socialuni.social.tance.config;


import com.socialuni.social.common.api.constant.DateTimeType;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class AppConfigDOKeyConst {
    public static final String wx_mp_id = "wx_mp_id";
//    public static final String wx_app_id = "wx_app_id";
    public static final String wx_mp_secret = "wx_mp_secret";
//    public static final String wx_merchant_id = "wx_merchant_id";
//    public static final String wx_merchant_key = "wx_merchant_key";
}
