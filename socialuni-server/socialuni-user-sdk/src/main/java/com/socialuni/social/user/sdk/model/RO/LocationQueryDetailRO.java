package com.socialuni.social.user.sdk.model.RO;

import lombok.Data;

@Data
public class LocationQueryDetailRO {
    //纬度
    private Double lat;
    //经度
    private Double lng;
}
