package com.socialuni.social.user.sdk.model.RO;

import lombok.Data;

@Data
public class RectangleVO {
    private Double lon;
    private Double lat;
    private String adCode;
}
